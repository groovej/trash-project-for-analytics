class TodoItemsController < ApplicationController
  before_action :find_to_do_list  

  def index
  end

  def new
   	@todo_item = @todo_list.todo_items.new
  end

  def create

  	@todo_item = @todo_list.todo_items.new(todo_item_params)
  	if @todo_item.save
  		flash[:success] = "Added new item to the list!"
  		redirect_to to_do_list_todo_items_path
  	else
  		flash[:error] = "There are some problems with additing item."
  		render action: :new
  	end
  end

  def edit 

    @todo_item = @todo_list.todo_items.find(params[:id])
  end

  def update 
    
    @todo_item = @todo_list.todo_items.find(params[:id])
    if @todo_item.update_attributes(todo_item_params)
      flash[:success] = "Saved item to the list!"
      redirect_to to_do_list_todo_items_path
    else
      flash[:error] = "There are some problems with saving item."
      render action: :edit
    end
  end

  def url_options
    { to_do_list_id: params[:to_do_list_id] }.merge(super)
  end

  def destroy
    @todo_item = @todo_list.todo_items.find(params[:id])
    if @todo_item.destroy
      flash[:success] = "Todo list item was deleted!"
    else
      flash[:error] = "Todo item couldn't be deleted from this list." 
    end
    redirect_to to_do_list_todo_items_path
  end

  def complete
    @todo_item = @todo_list.todo_items.find(params[:id])
    @todo_item.update_attribute(:complited_at, Time.now)
    redirect_to to_do_list_todo_items_path, notice: "Todo item marked as complited."
  end

  private
    def find_to_do_list
      @todo_list = ToDoList.find(params[:to_do_list_id])
    end


  	def todo_item_params
  		params[:todo_item].permit(:content)
  	end
end
