class TodoItem < ActiveRecord::Base
  belongs_to :to_do_list
  validates :content, presence: true, 
  					length: { minimum: 3 }
  def completed?
  	!complited_at.blank?
  end
end
