require 'spec_helper'

describe TodoItem do
  it { should belong_to(:to_do_list) }

  describe "#completed?" do
  	let(:todo_item) { TodoItem.create(content: "Hello") }

  	it "if false when complited_at false" do
  		todo_item.complited_at = nil
  		expect(todo_item.completed?).to be_falsey
  	end

  	it "if true when complited_at present" do
  		todo_item.complited_at = Time.now
  		expect(todo_item.completed?).to be_truthy
  	end

  end
end
