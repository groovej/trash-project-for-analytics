module TodoListHelpers
	def visit_to_do_list list
		visit "/to_do_lists"
		within dom_id_for(list) do
			click_link "List Items"
		end
	end
end