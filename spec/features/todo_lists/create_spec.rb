require 'spec_helper'

describe "Creating to_do List" do
	def create_to_do_list (opt={})
		opt[:title] ||= "My to Do List"
		opt[:description] ||= "This is my to do list"

		visit '/to_do_lists'
		click_link 'New To do list'
		expect(page).to have_content "New to_do_list"

		fill_in "Title", with: opt[:title]
		fill_in "Description", with: opt[:description]

		click_button "Create To do list"  
	end

	it "redirect to Index page on success" do
		create_to_do_list
		expect(page).to have_content('My to Do List') 
	end	

	it "displays error if no title there" do
		expect(ToDoList.count).to eq(0)

		create_to_do_list title: ""

		expect(ToDoList.count).to eq(0)

		visit "/to_do_lists"
		expect(page).to_not have_content("for doing things")

	end

	it "displays an error when the todo list has a title less than 3 characters" do
		expect(ToDoList.count).to eq(0)

		create_to_do_list title: "Hi"
		
		expect(page).to have_content('error') 
		expect(ToDoList.count).to eq(0)

		visit "/to_do_lists"
		expect(page).to_not have_content("for doing things")
	end

	it "displays error if no Description there" do
		expect(ToDoList.count).to eq(0)
		create_to_do_list description: ""

		expect(page).to have_content('error') 
		expect(ToDoList.count).to eq(0)

		visit "/to_do_lists"
		expect(page).to_not have_content("Grocery list")
	end

	it "displays an error when the todo list has a description less than 5 characters" do
		expect(ToDoList.count).to eq(0)
		create_to_do_list description: "for"

		expect(page).to have_content('error') 
		expect(ToDoList.count).to eq(0)

		visit "/to_do_lists"
		expect(page).to_not have_content("for doing things")
	end


end