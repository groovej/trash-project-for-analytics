require 'spec_helper'

describe "Editing to-do List" do 
	let!(:to_do_list){ToDoList.create(title: "Grocery", description: "Grocery list ")}

	def update_to_to_list(opt={})
		opt[:tilte] ||= "My to do list"
		opt[:description] ||= "This is my to do list"
		to_do_list = opt[:to_do_variable]

		visit "/to_do_lists"
		within "#to_do_list_#{to_do_list.id}" do
			click_link "Edit"
		end

		fill_in "Title", with: opt[:title]
		fill_in "Description", with: opt[:description]
		click_button "Update To do list"
	end

	it "update a to do list successfully" do
				
		update_to_to_list title:"New title",
						 description: "New Description",
						 to_do_variable: to_do_list

		to_do_list.reload

		expect(page).to have_content("To do list was successfully updated.")
		expect(to_do_list.title).to eq("New title")
		expect(to_do_list.description).to eq("New Description")
	end	

	it  "displays an error with no title" do
		update_to_to_list to_do_variable: to_do_list, title: ""
		title = to_do_list.title
		to_do_list.reload

		expect(to_do_list.title).to eq (title)
		expect(page).to have_content("error")
	end

	it  "displays an error with short title" do
		
		update_to_to_list to_do_variable: to_do_list, title: "Hi"

		expect(page).to have_content("error")
	end

	it  "displays an error with no description" do
		update_to_to_list to_do_variable: to_do_list, description: ""
		expect(page).to have_content("error")
	end

	it  "displays an error with less description" do
		update_to_to_list to_do_variable: to_do_list, description: "For"
		expect(page).to have_content("error")
	end


end