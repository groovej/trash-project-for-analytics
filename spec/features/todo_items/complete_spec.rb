require 'spec_helper'

describe "Completing to do item" do
	let!(:todo_list) {ToDoList.create(title: "Grocery list", description: "Groceries description")}
	let!(:todo_item) {todo_list.todo_items.create(content: "Buy milk")}

	it "is successful when marking a single item complete" do
		expect(todo_item.complited_at).to be_nil
		visit_to_do_list todo_list
		within dom_id_for(todo_item) do
			click_link "Mark Complete"
		end
		todo_item.reload
		expect(todo_item.complited_at).not_to be_nil
	end

	context "With complited items" do
		let!(:complited_todo_item) {todo_list.todo_items.create(content: "Buy eggs", complited_at: 5.minutes.ago)}
		it "shows completed item as complete" do
			visit_to_do_list todo_list
			within dom_id_for(complited_todo_item) do
				expect(page).to have_content(complited_todo_item.complited_at)
			end
		end

		it "doesn't give the option to complete if item complited" do
			visit_to_do_list todo_list
			within dom_id_for(complited_todo_item) do
				expect(page).not_to have_content("Mark Complete")
			end
		end

	end

end