require 'spec_helper'

describe "Adding to do items" do
	let!(:todo_list) {ToDoList.create(title: "Grocery list", description: "Groceries description")}
	
	it "is successful with valid content" do
		visit_to_do_list(todo_list)
		click_link "New Todo Item"
		fill_in "Content", with: "Milk"
		click_button "Save"
		expect(page).to have_content("Added new item to the list!")
		within("table.todo_list") do
			expect(page).to have_content("Milk")
		end
	end
	
	it "displays an error if there are no content" do
		visit_to_do_list(todo_list)
		click_link "New Todo Item"
		fill_in "Content", with: ""
		click_button "Save"
		within("div.flash") do
			expect(page).to have_content("There are some problems with additing item.")
		end
		expect(page).to have_content("Content can't be blank")
	end

	it "displays an error if there are small content (2 characters)" do
		visit_to_do_list(todo_list)
		click_link "New Todo Item"
		fill_in "Content", with: "tr"
		click_button "Save"
		within("div.flash") do
			expect(page).to have_content("There are some problems with additing item.")
		end
		expect(page).to have_content("Content is too short")
	end


end