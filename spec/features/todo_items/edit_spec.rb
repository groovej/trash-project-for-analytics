require 'spec_helper'

describe "Edit to do item" do
	let!(:todo_list) {ToDoList.create(title: "Grocery list", description: "Groceries description")}
	let!(:todo_item) {todo_list.todo_items.create(content: "Buy milk")}

	it "is successful with valid content" do
		visit_to_do_list(todo_list)
		within ("#todo_item_#{todo_item.id}") do
			click_link "Edit"
		end
		fill_in "Content", with: "Lots of Milk"
		click_button "Save"
		expect(page).to have_content("Saved item to the list!")
		todo_item.reload
		expect(todo_item.content).to eq("Lots of Milk")
	end

	it "is unsuccessful with no content" do
		visit_to_do_list(todo_list)
		within ("#todo_item_#{todo_item.id}") do
			click_link "Edit"
		end
		fill_in "Content", with: ""
		click_button "Save"
		expect(page).to_not have_content("Saved item to the list!")
		expect(page).to have_content("Content can't be blank")
		todo_item.reload
		expect(todo_item.content).to eq("Buy milk")
	end

	it "is unsuccessful with no enough content" do
		visit_to_do_list(todo_list)
		within ("#todo_item_#{todo_item.id}") do
			click_link "Edit"
		end
		fill_in "Content", with: "ku"
		click_button "Save"
		expect(page).not_to have_content("Saved item to the list!")
		expect(page).to have_content("Content is too short")
		todo_item.reload
		expect(todo_item.content).to eq("Buy milk")
	end
end