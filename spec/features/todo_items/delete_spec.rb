require 'spec_helper'

describe "Deleting to do item" do
	let!(:todo_list) {ToDoList.create(title: "Grocery list", description: "Groceries description")}
	let!(:todo_item) {todo_list.todo_items.create(content: "Buy milk")}
	
	it "is successful" do 
		visit_to_do_list(todo_list)
		within "#todo_item_#{todo_item.id}" do
			click_link "Delete"
		end
		expect(page).to have_content("Todo list item was deleted!")
		expect(TodoItem.count).to eq(0)
	end
end