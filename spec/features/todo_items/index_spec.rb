require 'spec_helper'

describe "Viewing to do items" do
	let!(:todo_list) {ToDoList.create(title: "Grocery list", description: "Groceries description")}

	it "displays the title of current to_do List" do
		visit_to_do_list(todo_list)
		within("h1.list_title") do
			expect(page).to have_content(todo_list.title)
		end
	end

	it "displays no items when a todo list is empty" do
		visit_to_do_list(todo_list)
		expect(page.all('table.todo_items tr').size).to eq(0)
	end

	it "displays item content when a to_do List has items" do 
		todo_list.todo_items.create(content: "Milk")
		todo_list.todo_items.create(content: "Bread")

		visit_to_do_list(todo_list)
		
		# expect(page.all('ul.todo_items li').size).to eq(2)

		within "table.todo_list" do
			expect(page).to have_content("Milk")
			expect(page).to have_content("Bread")
		end
	end
end